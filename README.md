# Teste Estágio - Game Design

Escolha um aplicativo e um jogo que você goste (ambos devem ser populares). Para cada um, monte um documento com plano de testes contendo prints de tela.

Por fim, proponha três melhorias para melhorar a experiência do usuário no app e três melhorias que facilitariam o processo de testes no game. Justifique sua resposta.

Responda com um doc, pdf ou um link para o google docs.